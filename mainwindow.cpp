#include "mainwindow.h"

#include <QApplication>
#include <QAction>
#include <QContextMenuEvent>
#include <QMenu>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    rightClickMenu = new QMenu(this);
    rightClickMenu->addAction(new QAction("Test", this));
    setAttribute(Qt::WA_AcceptTouchEvents);
}

MainWindow::~MainWindow()
{

}

void MainWindow::contextMenuEvent(QContextMenuEvent *event)
{
    rightClickMenu->exec(event->pos());
}

void MainWindow::mouseDoubleClickEvent(QMouseEvent */*event*/)
{
    QMessageBox::information(this, "Test", "Double click detected");

}
